package br.com.sempro;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-06T19:28:22")
@StaticMetamodel(Task.class)
public class Task_ { 

    public static volatile SingularAttribute<Task, Date> dtUpdate;
    public static volatile SingularAttribute<Task, Date> dtCriacao;
    public static volatile SingularAttribute<Task, String> titulo;
    public static volatile SingularAttribute<Task, Date> dtConclusao;
    public static volatile SingularAttribute<Task, Integer> id;
    public static volatile SingularAttribute<Task, String> descricao;
    public static volatile SingularAttribute<Task, Integer> status;

}